// console.log("Hello World");

// Using the exponent operator
	let number = 2;
	const getCube = number ** 3;
	console.log(`The cube of ${number} is ${getCube}`);

// String Using Template Literals - address
	let address = ['258', 'Washington Ave NW', 'California', 90011]
	
	const [houseNumber, street, state, zipCode] = address;
	messageOne = `I live at ${houseNumber} ${street} ${state} ${zipCode}`;
	console.log(messageOne);

// String Using Template Literals - animal
	let animal = ['Lolong', 'saltwater crocodile', 1075,'20 ft 3 in']
	
	const [name, specie, weight, measurement] = animal;
	messageTwo = `${name} was a ${specie}. He weighed ${weight} with a measurement of ${measurement}.`;
	console.log(messageTwo);

// forEach loop
	let numbers = [1,2,3,4,5]

	const sampleFunction = (item) => {
		console.log(`${item}`);
	}

	numbers.forEach(sampleFunction);

// reduceNumber
	let initialValue = 0;
	const reduceNumber = numbers.reduce( 
		(accumulator, currentValue) => accumulator + currentValue, initialValue
		);
	
	console.log(reduceNumber);


// class Dog
	class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

	const myNewDog = new Dog("Frankie", 5, "Miniature Dachshund");
	console.log(myNewDog);